#include <iostream>

using namespace std;

typedef long long i64;

const int MAX_N = 100000;

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

int n;
int k;

int a[MAX_N];

i64 sum_a(i64 upper)
{
    i64 sum = 0;

    for (int i = 0; i < n; i ++) {
        sum += min(a[i], upper);
    }

    return sum;
}

int max_a()
{
    int result = a[0];

    for (int i = 1; i < n; i ++) {
        result = max(result, a[i]);
    }

    return result;
}

i64 bsearch()
{
    i64 l = 1;
    i64 r = k;
    i64 m;

    while (l < r) {
        m = (l + r) >> 1;

        if (sum_a(m) >= k) {
            r = m;
        } else {
            l = m + 1;
        }
    }

    return l;
}

int main()
{
    cin >> n >> k;

    for (int i = 0; i < n; i ++) {
        cin >> a[i];
    }

    if (sum_a(max_a()) < k) {
        cout << -1;
        return 0;
    }

    i64 iter;
    i64 sa;
    i64 r;

    while (k > 0) {
        iter = bsearch();
        sa = sum_a(iter);

        if (sa > k) break;
        k -= sa;

        for (int i = 0; i < n; i ++) {
            a[i] -= min(a[i], iter);
        }
    }

    if (k > 0) {
        iter --;

        for (int i = 0; i < n; i ++) {
            r = min(a[i], iter);

            a[i] -= r;
            k -= r;
        }

        for (int i = 0; i < n; i ++) {
            if (k == 0) {
                if (a[i] > 0) {
                    cout << i + 1 << ' ';
                    a[i] = 0;
                }
            } else {
                r = min(a[i], 1);

                a[i] -= r;
                k -= r;
            }
        }
    }

    for (int i = 0; i < n; i ++) {
        if (a[i] > 0) {
            cout << i + 1 << ' ';
        }
    }

    return 0;
}
