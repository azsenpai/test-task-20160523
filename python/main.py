
def sum_a(upper):
    global n, a

    s = 0

    for i in range(n):
        s += min(a[i], upper)

    return s


def bsearch():
    global k, a

    l = 1
    r = k

    while l < r:
        m = (l + r) >> 1

        if sum_a(m) >= k:
            r = m
        else:
            l = m + 1

    return l


def main():
    global n, k, a

    n, k = map(int, input().split())
    a = list(map(int, input().split()))

    if sum_a(max(a)) < k:
        print(-1)
        return

    while k > 0:
        itern = bsearch()
        sa = sum_a(itern)

        if sa > k: break
        k -= sa

        for i in range(n):
            a[i] -= min(a[i], itern)

    if k > 0:
        itern -= 1

        for i in range(n):
            r = min(a[i], itern)

            a[i] -= r
            k -= r

        for i in range(n):
            if k == 0:
                if a[i] > 0:
                    print(i+1, end = ' ')
                    a[i] = 0
            else:
                r = min(a[i], 1)

                a[i] -= r
                k -= r

    for i in range(n):
        if a[i] > 0:
            print(i+1, end = ' ')


if __name__ == '__main__':
    main()
