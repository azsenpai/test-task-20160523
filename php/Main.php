<?php

list($n, $k) = explode(' ', fgets(STDIN));

$n = (int)$n;
$k = (int)$k;

$a = explode(' ', fgets(STDIN));

if (sum_a(max($a)) < $k) {
    print(-1);
    exit();
}

while ($k > 0) {
    $iter = bsearch();
    $sum = sum_a($iter);

    if ($sum > $k) {
        break;
    }
    $k -= $sum;

    for ($i = 0; $i < $n; $i ++) {
        $a[$i] -= min($a[$i], $iter);
    }
}

if ($k > 0) {
    $iter = $iter - 1;

    for ($i = 0; $i < $n; $i ++) {
        $r = min($a[$i], $iter);

        $a[$i] -= $r;
        $k -= $r;
    }

    for ($i = 0; $i < $n; $i ++) {
        if ($k == 0) {
            if ($a[$i] > 0) {
                print($i+1 . ' ');
                $a[$i] = 0;
            }
        } else {
            $r = min($a[$i], 1);

            $a[$i] -= $r;
            $k -= $r;
        }
    }
}

for ($i = 0; $i < $n; $i ++) {
    if ($a[$i] > 0) {
        print($i+1 . ' ');
    }
}


function sum_a($iter)
{
    global $n, $a;

    $sum = 0;

    for ($i = 0; $i < $n; $i ++) {
        $sum += ($a[$i] < $iter) ? $a[$i] : $iter;
    }

    return $sum;
}

function bsearch()
{
    global $k, $a;

    $l = 1;
    $r = $k;

    while ($l < $r) {
        $m = ($l + $r) >> 1;

        if (sum_a($m) >= $k) {
            $r = $m;
        } else {
            $l = $m + 1;
        }
    }

    return $l;
}
